/**
 * 
 */
package com.dice.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.dice.PageFactory.OR_dashboard;
import com.dice.core.Page;

/**
 * @author Avinash
 *
 */
public class dashBoard extends Page {

	/*
	 * private WebDriver driver;
	 * 
	 * 
	 * public dashBoard(WebDriver driver) { super(); this.driver = driver; }
	 */

	public dashBoard()  {
		super(driver);
	}

	OR_dashboard dashboard = PageFactory.initElements(driver, OR_dashboard.class);

	public void alertOutCheck() {
		//driver.findElement(By.xpath("//div[@id='searchable-confirmation-dashboard']/div/div/div[3]/button[2]")).click();
		dashboard.popSearchableNo.click();
		dashboard.jobstab.click();
		dashboard.alerttab.click();
		dashboard.alertgetCheck();

	}

}
