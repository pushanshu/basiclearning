/**
 * 
 */
package com.dice.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author avinash.sharma
 *
 */
public class OR_TopNavigation {
	private int timeout = 30;
	

	@FindBy(css = "#smart-toggle-link")
	public WebElement HeaderUserName;
	@FindBy(css = "#smart-toggle > li:nth-child(6) > form > button")
	public WebElement oSignOut;
}
