/**
 * 
 */
package com.dice.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;

import com.dice.page.TopNavigation;

/**
 * @author Avinash
 *
 */
public class Page {

	public static WebDriver driver;
	public static String baseUrl;
	public static Properties Config = new Properties();
	public static Properties OR = new Properties();
	public static FileInputStream fis;
	/*
	 * public static Xls_Reader excel = new Xls_Reader(
	 * System.getProperty("user.dir")+
	 * File.separator+"src"+File.separator+"test"+File.separator+"java"+File.
	 * separator+"com"+File.separator+"dice"+File.separator+"testdata"+File.
	 * pathSeparator+"test.xlsx");
	 */ public static Logger logs = Logger.getLogger("devpinoyLogger");
	public static TopNavigation topNav;
	public Page(WebDriver driver) {
		Page.driver=driver;
	}

	public static void openBrowser() {

		if (driver == null) {

			try {
				fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator
						+ "test" + File.separator + "java" + File.separator + "com" + File.separator + "dice"
						+ File.separator + "config" + File.separator + "config.properties");
			} catch (FileNotFoundException e) {
				System.out.println(e);
				e.printStackTrace();
			}
			try {
				Config.load(fis);
			} catch (IOException e) {
				System.out.println("Fail to read properity file");
				e.printStackTrace();
			}
			logs.debug("Loaded the Config property file");

			try {
				fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator
						+ "test" + File.separator + "java" + File.separator + "com" + File.separator + "dice"
						+ File.separator + "config" + File.separator + "OR.properties");
			} catch (FileNotFoundException e) {
				System.out.println(e);
				e.printStackTrace();
			}
			try {
				OR.load(fis);
			} catch (IOException e) {
				System.out.println(e);
				e.printStackTrace();
			}
			logs.debug("loaded the OR property file");

			if (Config.getProperty("browser").equals("firefox")) {

				driver = new FirefoxDriver();
				logs.debug("Loaded Firefox");

			} else if (Config.getProperty("browser").equals("ie")) {

				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				driver = new InternetExplorerDriver();

			} else if (Config.getProperty("browser").equals("chrome")) {

				System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
				driver = new ChromeDriver();

			}
			baseUrl = Config.getProperty("testsiteurl");
			driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
			// DbManager.setMysqlDbConnection();
			topNav = new TopNavigation(driver);

		}

	}

	/*
	 * public static void click(String key) {
	 * 
	 * driver.findElement(By.xpath(OR.getProperty(key))).click();
	 * System.out.println(key);
	 * 
	 * }
	 * 
	 * public static void input(String key, String value) {
	 * 
	 * driver.findElement(By.xpath(OR.getProperty(key))).sendKeys(value);
	 * 
	 * }
	 */
	@AfterSuite
	// Test cleanup
	public void TeardownTest() {
		driver.quit();
	}

	public static void targetURL(String target) {
		 driver.navigate().to(baseUrl+target);
		
	}
	public void BrowerCloseTest() {
		driver.close();
	}

}
