package com.dice.page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.dice.PageFactory.OR_Djv;
import com.dice.PageFactory.OR_SearchPage;
import com.dice.core.Page;


public class Djv extends Page {
	public Djv() {
		super(driver);
		}
	OR_Djv djv = PageFactory.initElements(driver, OR_Djv.class);
/**
 * @param email
 * @param password
 * @return
 */
	public Djv signInDjv(String email,String password) {
		delay();
		djv.oUserEmail.clear();
		djv.oUserEmail.sendKeys(email);
		djv.oPassword.clear();
		djv.oPassword.sendKeys(password);
		djv.oSignIn.click();delay();delay();
		return PageFactory.initElements(driver, Djv.class);
		}
	
	public Djv applicationCanel() {
		djv.oCancelApplication.click();
		System.out.println("clicked on the cancel application form");
		return PageFactory.initElements(driver, Djv.class);
	}
	public Djv applyButton() {
		djv.oApplyButton.click();
		System.out.println("clicked on the Apply Button");
		return PageFactory.initElements(driver, Djv.class);
	}
	public Djv Verifyurlcompany() {
		delay();
		System.out.println(driver.getCurrentUrl());
		Page.switchToTab();
		System.out.println("Redirecting dice URLjob djv");
		return PageFactory.initElements(driver, Djv.class);
	}

	
}


