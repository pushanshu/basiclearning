/**
 * 
 */
package com.dice.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dice.core.Page;
import com.dice.page.dashBoard;

/**
 * @author Avinash
 *
 */
public class OR_SeekerLoginPage extends Page {
	
	public OR_SeekerLoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/*private WebDriver driver;

	public OR_SeekerLoginPage(WebDriver driver) {
		this.driver=driver;
	}
*/
	@FindBy(xpath = "//*[@id=\"email\"]")
	@CacheLookup
	private WebElement seekerEmail;
	
	@FindBy(xpath = "//*[@id=\"password\"]")
	@CacheLookup
	private WebElement seekerPassword;
	
	@FindBy(xpath = "//*[@id=\"loginDataSubmit\"]/div[3]/div/button")
	@CacheLookup
	private WebElement signIn;
	
	/**
	 * seeker login
	 * @param useremail
	 * @param userpassword
	 */
	public dashBoard dologin(String useremail,String userpassword) {
		seekerEmail.clear();
		seekerEmail.sendKeys(useremail);
		seekerPassword.clear();
		seekerPassword.sendKeys(userpassword);
		signIn.click();
		
		return PageFactory.initElements(driver, dashBoard.class);
		
	}

}
