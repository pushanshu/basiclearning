/**
 * 
 */
package com.dice.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author avinash.sharma
 *
 */
public class OR_Djv {
	private int timeout = 30;
	

	@FindBy(xpath = "(//button[@type='button'])[4]")
	public WebElement oApplyButton;
	@FindBy(id = "mEmailQuick")
	public WebElement oUserEmail;
	@FindBy(id = "mPasswordQuick")
	public WebElement oPassword;
	@FindBy(id = "easyapplyJob-log-in-btn")
	public WebElement oSignIn;
	
	@FindBy(css = "#cover-resume-modal > div > div > div.modal-header > div > div.col-sm-1 > button")
	public WebElement oApplicationCanel;

}
