package com.dice.page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dice.PageFactory.OR_SearchPage;
import com.dice.core.Page;

/**
 * 
 * @author Avinash
 *
 */

public class searchPage extends Page {
	
	
	public searchPage() {
		super(driver);
			}
	OR_SearchPage searchpage = PageFactory.initElements(driver, OR_SearchPage.class);
/**
 * Blank search on the serp
 */
	public void blankSearch() {
		searchpage.oJobTitle.clear();
		searchpage.inputLocation.clear();
		searchpage.clickFindTechJobsButton();
		

	}
/**
 * Location search take input of the keyword
 * @param keyword
 */
	
	  public void loacationSearch(String location) {
	  searchpage.oJobTitle.clear(); 
	  searchpage.inputLocation.sendKeys(location);
	  searchpage.clickFindTechJobsButton();
	  }
	 
	/**
	 * Location search with default keyword product manager
	 */
	  public void loacationSearch() {
		
		searchpage.oJobTitle.clear();
		searchpage.inputKeyword.sendKeys("Product manager");
		searchpage.inputLocation.clear();
		searchpage.clickFindTechJobsButton();
	}
	  
		public void verifySortByDatePresent(){
			Assert.assertTrue("relevance is NOT selected by default",driver.findElement(By.cssSelector("a#sort-by-date-link")).isDisplayed());
			System.out.println("Verified Sort By Date is a link");
		}
		
		public void verifyRelevanceLinkIsPresent(){
			Assert.assertTrue("Relevance link is NOT present",driver.findElement(By.cssSelector("a#sort-by-relevance-link")).isDisplayed());
			System.out.println("Verified Relevance is a link");

		}
		
		public void verifyDistanceLinkIsPresent(){
			Assert.assertTrue("Distance link is NOT present",driver.findElement(By.cssSelector("a#sort-by-distance-link")).isDisplayed());
			System.out.println("Verified Sort By Distance is a link");
		}
		
		public void sortByDistanceClick(){
			searchpage.oSortByDaistanceLink.click();
		}
		public void sortByRelevanceClick(){
			searchpage.oSortByRelevanceLink.click();
		}
		
		public void sortByDateClick(){
			searchpage.oSortByDateLink.click();
		}
		public void inputOnlyKeyword(String keyword) {
			searchpage.oJobTitle.clear();
			searchpage.oJobTitle.sendKeys(keyword);;
			searchpage.inputLocation.clear();
			searchpage.clickFindTechJobsButton();
			
		}

}