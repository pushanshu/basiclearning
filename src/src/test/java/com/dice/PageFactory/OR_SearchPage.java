package com.dice.PageFactory;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class OR_SearchPage {
	//private Map<String, String> data;
   // private WebDriver driver;
	private int timeout = 30;
	/**
	 * Click on Find Tech Jobs Button.
	 *
	 * @return the OR_SearchPage class instance.
	 */
	public OR_SearchPage clickFindTechJobsButton() {
		findTechJobs.click();
		return this;
	}

	@FindBy(xpath = "//*[@id='JOBDATA']/div[6]/select")
	public WebElement oJobContact;
	@FindBy(xpath = "//*[@id='JOBDATA']/div[9]/input")
	public WebElement oJobNumber;
	@FindBy(id = "job")
	public WebElement oJobTitle;
	/*@FindBy(id = "job")
	public WebElement oActiveJobs;*/
	@FindBy(xpath = "//*[@id='JOBDATA']/div[24]/input")
	public WebElement oInctiveJobs;
	@FindBy(xpath = "//*[@id='JOBDATA']/div[27]/input")
	public WebElement oAllJobs;
	@FindBy(xpath = "//*[@id='CONTINUE-button']")
	public WebElement oContinue;
	
	//Filters
		@FindBy(css = "a#sort-by-relevance-link")
		public WebElement oSortByRelevanceLink;
		
		@FindBy(css = "#sort-by-date-link")
		public WebElement oSortByDateLink;
		
		@FindBy(css = "#sort-by-distance-link")
		public WebElement oSortByDaistanceLink;

	@FindBy(css = "#job")
	//@CacheLookup
	public WebElement inputKeyword;

	@FindBy(css = "#location")
	//@CacheLookup
	public WebElement inputLocation;

	@FindBy(css = "#viewedId56908d346ebfa97f5e49a281849af772 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a1;

	@FindBy(css = "#viewedId20127342dc0584190c9bb5a5baacf826 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a2;

	@FindBy(css = "#viewedId2a0a6246197f51183cef7e95a2095d84 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a3;

	@FindBy(css = "#viewedId479badcdd84ab01a5f9150a54971500d div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a4;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(8) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a5;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(13) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a6;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(15) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a7;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(27) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement a8;

	@FindBy(css = "a[href='http://insights.dice.com/about-dice/']")
	//@CacheLookup
	private WebElement aboutUs;

	@FindBy(id = "advanced-search-link")
	//@CacheLookup
	private WebElement advancedSearch;

	@FindBy(id = "company7")
	//@CacheLookup
	private WebElement aeBusiness1;

	@FindBy(id = "company12")
	//@CacheLookup
	private WebElement aeBusiness2;

	@FindBy(id = "company14")
	//@CacheLookup
	private WebElement aeBusiness3;

	@FindBy(id = "company26")
	//@CacheLookup
	private WebElement aeBusiness4;

	@FindBy(id = "company7")
	//@CacheLookup
	private WebElement aeBusinessSolutions1;

	@FindBy(id = "company12")
	//@CacheLookup
	private WebElement aeBusinessSolutions10;

	@FindBy(id = "company14")
	//@CacheLookup
	private WebElement aeBusinessSolutions11;

	@FindBy(id = "company26")
	//@CacheLookup
	private WebElement aeBusinessSolutions12;

	@FindBy(id = "company7")
	//@CacheLookup
	private WebElement aeBusinessSolutions2;

	@FindBy(id = "company12")
	//@CacheLookup
	private WebElement aeBusinessSolutions3;

	@FindBy(id = "company12")
	//@CacheLookup
	private WebElement aeBusinessSolutions4;

	@FindBy(id = "company14")
	//@CacheLookup
	private WebElement aeBusinessSolutions5;

	@FindBy(id = "company14")
	//@CacheLookup
	private WebElement aeBusinessSolutions6;

	@FindBy(id = "company26")
	//@CacheLookup
	private WebElement aeBusinessSolutions7;

	@FindBy(id = "company26")
	//@CacheLookup
	private WebElement aeBusinessSolutions8;

	@FindBy(id = "company7")
	//@CacheLookup
	private WebElement aeBusinessSolutions9;

	@FindBy(css = "a.app-store-link.app-store")
	//@CacheLookup
	private WebElement appStore;

	@FindBy(css = "a[href='/jobs/l-Atlanta%2C_GA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement atlantaGa;

	@FindBy(css = "a[href='/jobs/l-Austin%2C_TX-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement austinTx;

	@FindBy(id = "position8")
	//@CacheLookup
	private WebElement automationEngineer1;

	@FindBy(id = "position8")
	//@CacheLookup
	private WebElement automationEngineer2;

	@FindBy(css = "#viewedId68034b0e8a0aa44e1509c108d1d30cc4 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement b1;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement b2;

	@FindBy(id = "company0")
	//@CacheLookup
	private WebElement bereanGroup;

	@FindBy(id = "company0")
	//@CacheLookup
	private WebElement bereanGroupInternationalInc1;

	@FindBy(id = "company0")
	//@CacheLookup
	private WebElement bereanGroupInternationalInc2;

	@FindBy(id = "company0")
	//@CacheLookup
	private WebElement bereanGroupInternationalInc3;

	@FindBy(css = "a[href='/jobs/l-Boston%2C_MA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement bostonMa;

	@FindBy(css = "a[href='/jobs/browsejobs']")
	//@CacheLookup
	private WebElement browseJobs;

	@FindBy(id = "position21")
	//@CacheLookup
	private WebElement buildReleaseEngineer1;

	@FindBy(id = "position21")
	//@CacheLookup
	private WebElement buildReleaseEngineer2;

	@FindBy(css = "a[href='/jobs/djt-Business_Analyst-sort-date-jobs']")
	//@CacheLookup
	private WebElement businessAnalyst;

	@FindBy(css = "a[href='/jobs/jtype-C2H_Corp%26%2345To%26%2345Corp-sort-date-jobs']")
	//@CacheLookup
	private WebElement c2hCorptocorp;

	@FindBy(css = "a[href='/jobs/jtype-C2H_Independent-sort-date-jobs']")
	//@CacheLookup
	private WebElement c2hIndependent;

	@FindBy(css = "a[href='/jobs/jtype-C2H_W2-sort-date-jobs']")
	//@CacheLookup
	private WebElement c2hW2;

	@FindBy(css = "a[href='/jobs/dc-Capital_Markets_Placement-sort-date-jobs']")
	//@CacheLookup
	private WebElement capitalMarketsPlacement;

	@FindBy(id = "smart-toggle-Career-Explorer")
	//@CacheLookup
	private WebElement careerExplorer1;

	@FindBy(id = "smart-toggle-Career-Explorer")
	//@CacheLookup
	private WebElement careerExplorer2;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement careerPaths1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement careerPaths2;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement careerToolkitArticles1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement careerToolkitArticles2;

	@FindBy(css = "a[href='/jobs/l-Charlotte%2C_NC-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement charlotteNc;

	@FindBy(css = "a[href='/jobs/l-Chicago%2C_IL-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement chicagoIl;

	@FindBy(css = "a[href='http://www.google.co.uk/doubleclick']")
	//@CacheLookup
	private WebElement clickHere;

	@FindBy(css = "a.btn.btn-primary")
	//@CacheLookup
	private WebElement close1;

	@FindBy(id = "CloseCookieConsentPolicy")
	//@CacheLookup
	private WebElement close2;

	@FindBy(css = "a[href='http://media.dice.com/']")
	//@CacheLookup
	private WebElement companyProfile;

	@FindBy(css = "a[href='http://techhub.dice.com/Dice_General-ContactUs_D.html']")
	//@CacheLookup
	private WebElement contactSales;

	@FindBy(css = "a[href='/about/contactus']")
	//@CacheLookup
	private WebElement contactUs;

	@FindBy(id = "position13")
	//@CacheLookup
	private WebElement contentManagementSystems1;

	@FindBy(id = "position13")
	//@CacheLookup
	private WebElement contentManagementSystems2;

	@FindBy(id = "continue")
	//@CacheLookup
	private WebElement continueToDiceUk;

	@FindBy(css = "button.btn.btn-primary.pull-right")
	//@CacheLookup
	private WebElement continueWithTheNewSite;

	@FindBy(css = "a[href='/jobs/jtype-Contract_Corp%26%2345To%26%2345Corp-sort-date-jobs']")
	//@CacheLookup
	private WebElement contractCorptocorp;

	@FindBy(css = "a[href='/jobs/jtype-Contract_Independent-sort-date-jobs']")
	//@CacheLookup
	private WebElement contractIndependent;

	@FindBy(css = "a[href='/jobs/jtype-Contract_W2-sort-date-jobs']")
	//@CacheLookup
	private WebElement contractW2;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(3) a.hjtype.tjth")
	//@CacheLookup
	private WebElement contracts1;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(3) a.bindChkBox")
	//@CacheLookup
	private WebElement contracts2;

	@FindBy(css = "button.btn.btn-primary.btn-lg.modalButton")
	//@CacheLookup
	private WebElement createAlert;

	@FindBy(css = "a.btn.btn-lg.dice-btn-secondary.job-alert.pull-right")
	//@CacheLookup
	private WebElement createJobAlert1;

	@FindBy(css = "#resultSec div:nth-of-type(1) div:nth-of-type(4) div.col-md-12 a:nth-of-type(2)")
	//@CacheLookup
	private WebElement createJobAlert2;

	@FindBy(id = "position1")
	//@CacheLookup
	private WebElement creditProcessor1;

	@FindBy(id = "position1")
	//@CacheLookup
	private WebElement creditProcessor2;

	@FindBy(id = "position11")
	//@CacheLookup
	private WebElement customerServiceFrontLine1;

	@FindBy(id = "position11")
	//@CacheLookup
	private WebElement customerServiceFrontLine2;

	@FindBy(id = "position2")
	//@CacheLookup
	private WebElement customerServiceRepresentative1;

	@FindBy(id = "position15")
	//@CacheLookup
	private WebElement customerServiceRepresentative2;

	@FindBy(id = "position2")
	//@CacheLookup
	private WebElement customerServiceRepresentative3;

	@FindBy(id = "position15")
	//@CacheLookup
	private WebElement customerServiceRepresentative4;

	@FindBy(id = "position3")
	//@CacheLookup
	private WebElement customerSolutionsAdvocateNoky1;

	@FindBy(id = "position3")
	//@CacheLookup
	private WebElement customerSolutionsAdvocateNoky2;

	@FindBy(id = "position27")
	//@CacheLookup
	private WebElement customerSuccessManager1;

	@FindBy(id = "position27")
	//@CacheLookup
	private WebElement customerSuccessManager2;

	@FindBy(css = "a[href='/jobs/dc-CyberCoders-sort-date-jobs']")
	//@CacheLookup
	private WebElement cybercoders;

	@FindBy(css = "a[href='/jobs/l-Dallas%2C_TX-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement dallasTx;

	@FindBy(css = "a[href='/jobs/dc-Deloitte-sort-date-jobs']")
	//@CacheLookup
	private WebElement deloitte;

	@FindBy(css = "a[href='/jobs/djt-DevOps_Engineer-sort-date-jobs']")
	//@CacheLookup
	private WebElement devopsEngineer;

	@FindBy(css = "a[href='http://www.dhigroupinc.com']")
	//@CacheLookup
	private WebElement dhiService;

	@FindBy(css = "footer.dice-footer div:nth-of-type(1) div.row div:nth-of-type(1) a.logo")
	//@CacheLookup
	private WebElement dice;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement diceCareersMobile1;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement diceCareersMobile2;

	@FindBy(css = "a[href='http://de.dice.com/']")
	//@CacheLookup
	private WebElement diceDe;

	@FindBy(css = "a[href='http://sap.dice.com/']")
	//@CacheLookup
	private WebElement diceSap;

	@FindBy(css = "a.diceUkAnchor")
	//@CacheLookup
	private WebElement diceUk;

	@FindBy(css = "a[href='/jobs/dcs-DirectHire-sort-date-jobs']")
	//@CacheLookup
	private WebElement directhire;

	@FindBy(id = "position18")
	//@CacheLookup
	private WebElement directorEngineering1;

	@FindBy(id = "position18")
	//@CacheLookup
	private WebElement directorEngineering2;

	@FindBy(id = "sort-by-distance-link")
	//@CacheLookup
	private WebElement distance;

	@FindBy(css = "a[href='/jobs/dc-Eliassen_Group-sort-date-jobs']")
	//@CacheLookup
	private WebElement eliassenGroup;

	@FindBy(id = "alertEmail")
	//@CacheLookup
	private WebElement email1;

	@FindBy(id = "mEmail")
	//@CacheLookup
	private WebElement email2;

	@FindBy(css = "a[href='/jobs/dc-Experis-sort-date-jobs']")
	//@CacheLookup
	private WebElement experis;

	@FindBy(css = "a.social-icon.facebook")
	//@CacheLookup
	private WebElement facebook;

	@FindBy(css = "a.btn.dice-btn-active.hidden-md.hidden-lg")
	//@CacheLookup
	private WebElement filter1;

	@FindBy(css = "#resultSec div:nth-of-type(1) div:nth-of-type(4) div.col-md-12 a:nth-of-type(1)")
	//@CacheLookup
	private WebElement filter2;

	@FindBy(css = "input.btn.btn-lg.btn-primary.btn-block.dice-btn.mB5")
	//@CacheLookup
	private WebElement findTechJobs;

	@FindBy(css = "#apply-job-modal-form div:nth-of-type(4) div:nth-of-type(1) p.pull-right a")
	//@CacheLookup
	private WebElement forgotPassword1;

	@FindBy(css = "#apply-job-modal-form div:nth-of-type(4) div:nth-of-type(4) p.pull-right a")
	//@CacheLookup
	private WebElement forgotPassword2;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(1) a.hjtype.tjth")
	//@CacheLookup
	private WebElement fulltime1;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(1) a.bindChkBox")
	//@CacheLookup
	private WebElement fulltime2;

	@FindBy(css = "#viewedIdddb07176940f41f2a282cd31508f25bb div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g1;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(4) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g10;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(9) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g11;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(12) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g12;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(16) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g13;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(22) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g14;

	@FindBy(css = "#viewedId89957440695a1e99c83f5a2585831625 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g2;

	@FindBy(css = "#viewedId2d2b7e0a17f4fa07c9e826c9f3760dd2 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g3;

	@FindBy(css = "#viewedIddab44a6cbc31c05aead6a7fa1a1f1733 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g4;

	@FindBy(css = "#viewedId763ec4732c678d627f81d87a9cbf5aff div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g5;

	@FindBy(css = "#viewedIda51cee8ad876446875fb236864150d4f div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g6;

	@FindBy(css = "#viewedIdbb24acd8f68790a27dd4862a9dc37dbc div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g7;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(2) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g8;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(3) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement g9;

	@FindBy(css = "a.app-store-link.google-play")
	//@CacheLookup
	private WebElement googlePlay;

	@FindBy(css = "a.social-icon.google-plus")
	//@CacheLookup
	private WebElement googlePlus;

	@FindBy(css = "a[href='http://tools.google.com/dlpage/gaoptout?hl=en']")
	//@CacheLookup
	private WebElement googlesPrivacySite;

	@FindBy(id = "company1")
	//@CacheLookup
	private WebElement greensky1;

	@FindBy(id = "company2")
	//@CacheLookup
	private WebElement greensky2;

	@FindBy(id = "company3")
	//@CacheLookup
	private WebElement greensky3;

	@FindBy(id = "company8")
	//@CacheLookup
	private WebElement greensky4;

	@FindBy(id = "company11")
	//@CacheLookup
	private WebElement greensky5;

	@FindBy(id = "company15")
	//@CacheLookup
	private WebElement greensky6;

	@FindBy(id = "company21")
	//@CacheLookup
	private WebElement greensky7;

	@FindBy(id = "company1")
	//@CacheLookup
	private WebElement greenskyCredit1;

	@FindBy(id = "company11")
	//@CacheLookup
	private WebElement greenskyCredit10;

	@FindBy(id = "company15")
	//@CacheLookup
	private WebElement greenskyCredit11;

	@FindBy(id = "company15")
	//@CacheLookup
	private WebElement greenskyCredit12;

	@FindBy(id = "company21")
	//@CacheLookup
	private WebElement greenskyCredit13;

	@FindBy(id = "company21")
	//@CacheLookup
	private WebElement greenskyCredit14;

	@FindBy(id = "company1")
	//@CacheLookup
	private WebElement greenskyCredit15;

	@FindBy(id = "company2")
	//@CacheLookup
	private WebElement greenskyCredit16;

	@FindBy(id = "company3")
	//@CacheLookup
	private WebElement greenskyCredit17;

	@FindBy(id = "company8")
	//@CacheLookup
	private WebElement greenskyCredit18;

	@FindBy(id = "company11")
	//@CacheLookup
	private WebElement greenskyCredit19;

	@FindBy(id = "company1")
	//@CacheLookup
	private WebElement greenskyCredit2;

	@FindBy(id = "company15")
	//@CacheLookup
	private WebElement greenskyCredit20;

	@FindBy(id = "company21")
	//@CacheLookup
	private WebElement greenskyCredit21;

	@FindBy(id = "company2")
	//@CacheLookup
	private WebElement greenskyCredit3;

	@FindBy(id = "company2")
	//@CacheLookup
	private WebElement greenskyCredit4;

	@FindBy(id = "company3")
	//@CacheLookup
	private WebElement greenskyCredit5;

	@FindBy(id = "company3")
	//@CacheLookup
	private WebElement greenskyCredit6;

	@FindBy(id = "company8")
	//@CacheLookup
	private WebElement greenskyCredit7;

	@FindBy(id = "company8")
	//@CacheLookup
	private WebElement greenskyCredit8;

	@FindBy(id = "company11")
	//@CacheLookup
	private WebElement greenskyCredit9;

	@FindBy(id = "hideDelMsg")
	//@CacheLookup
	private WebElement hideThisMessage;

	@FindBy(css = "a[href='/jobs/l-Houston%2C_TX-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement houstonTx;

	@FindBy(css = "#viewedId7b659ea82ad0a37ac71c48ff07210cdc div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i1;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(11) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i10;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(14) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i11;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(17) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i12;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(23) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i13;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(28) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i14;

	@FindBy(css = "#viewedIdfed03b42f3d7865f84766d2fae25ed92 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i2;

	@FindBy(css = "#viewedId80daa4d75d87aff97b1b5322ccc23ed1 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i3;

	@FindBy(css = "#viewedIda75e24bb475cfbb0ac285f8a3711b328 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i4;

	@FindBy(css = "#viewedId9552885a9c0107e9c4067591f1a6f45b div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i5;

	@FindBy(css = "#viewedIdecf003892b34673f494ce899b66e08cc div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i6;

	@FindBy(css = "#viewedId9843b78dbcbb14ac9af9f1ce3e15a4c8 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i7;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(7) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i8;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(10) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement i9;

	@FindBy(id = "agreePolicyModal")
	//@CacheLookup
	private WebElement iAccept;

	@FindBy(id = "company6")
	//@CacheLookup
	private WebElement information1;

	@FindBy(id = "company9")
	//@CacheLookup
	private WebElement information2;

	@FindBy(id = "company10")
	//@CacheLookup
	private WebElement information3;

	@FindBy(id = "company13")
	//@CacheLookup
	private WebElement information4;

	@FindBy(id = "company16")
	//@CacheLookup
	private WebElement information5;

	@FindBy(id = "company22")
	//@CacheLookup
	private WebElement information6;

	@FindBy(id = "company27")
	//@CacheLookup
	private WebElement information7;

	@FindBy(id = "company6")
	//@CacheLookup
	private WebElement informationBuilders1;

	@FindBy(id = "company16")
	//@CacheLookup
	private WebElement informationBuilders10;

	@FindBy(id = "company22")
	//@CacheLookup
	private WebElement informationBuilders11;

	@FindBy(id = "company22")
	//@CacheLookup
	private WebElement informationBuilders12;

	@FindBy(id = "company27")
	//@CacheLookup
	private WebElement informationBuilders13;

	@FindBy(id = "company27")
	//@CacheLookup
	private WebElement informationBuilders14;

	@FindBy(id = "company6")
	//@CacheLookup
	private WebElement informationBuilders15;

	@FindBy(id = "company9")
	//@CacheLookup
	private WebElement informationBuilders16;

	@FindBy(id = "company10")
	//@CacheLookup
	private WebElement informationBuilders17;

	@FindBy(id = "company13")
	//@CacheLookup
	private WebElement informationBuilders18;

	@FindBy(id = "company16")
	//@CacheLookup
	private WebElement informationBuilders19;

	@FindBy(id = "company6")
	//@CacheLookup
	private WebElement informationBuilders2;

	@FindBy(id = "company22")
	//@CacheLookup
	private WebElement informationBuilders20;

	@FindBy(id = "company27")
	//@CacheLookup
	private WebElement informationBuilders21;

	@FindBy(id = "company9")
	//@CacheLookup
	private WebElement informationBuilders3;

	@FindBy(id = "company9")
	//@CacheLookup
	private WebElement informationBuilders4;

	@FindBy(id = "company10")
	//@CacheLookup
	private WebElement informationBuilders5;

	@FindBy(id = "company10")
	//@CacheLookup
	private WebElement informationBuilders6;

	@FindBy(id = "company13")
	//@CacheLookup
	private WebElement informationBuilders7;

	@FindBy(id = "company13")
	//@CacheLookup
	private WebElement informationBuilders8;

	@FindBy(id = "company16")
	//@CacheLookup
	private WebElement informationBuilders9;

	@FindBy(css = "a[href='/jobs/dc-Infosys-sort-date-jobs']")
	//@CacheLookup
	private WebElement infosys;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement insights1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(1) li:nth-of-type(3) a")
	//@CacheLookup
	private WebElement insights2;

	@FindBy(id = "position22")
	//@CacheLookup
	private WebElement javaApplicationDeveloper1;

	@FindBy(id = "position22")
	//@CacheLookup
	private WebElement javaApplicationDeveloper2;

	@FindBy(css = "a[href='/jobs/djt-Java_Developer-sort-date-jobs']")
	//@CacheLookup
	private WebElement javaDeveloper;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement jobSearch1;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement jobSearch2;

	@FindBy(id = "job")
	//@CacheLookup
	private WebElement jobTitleSkillsKeywordsOrCompany;

	@FindBy(id = "position26")
	//@CacheLookup
	private WebElement juniorBiEngineer1;

	@FindBy(id = "position26")
	//@CacheLookup
	private WebElement juniorBiEngineer2;

	@FindBy(css = "#viewedId17afb960086a7661e49c0dc8749efc7e div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k1;

	@FindBy(css = "#viewedIdb2bfc4e4df675190877d7b6bc14bae09 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k10;

	@FindBy(css = "#viewedId1813dbbb83a5b9f6f8a7d0328ce7e731 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k11;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(5) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k12;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(6) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k13;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(18) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k14;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(19) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k15;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(20) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k16;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(21) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k17;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(24) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k18;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(25) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k19;

	@FindBy(css = "#viewedId58d9d01580ca3e675f96f59566c82b69 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k2;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(26) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k20;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(29) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k21;

	@FindBy(css = "#OR_SearchPage div:nth-of-type(30) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k22;

	@FindBy(css = "#viewedId3426e00c4af1a259d22460c5af921715 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k3;

	@FindBy(css = "#viewedId4750d4274261423d04734a5500aa4c98 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k4;

	@FindBy(css = "#viewedIdd9a36ff6faf5b1bbbde9690004495a4c div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k5;

	@FindBy(css = "#viewedIddffc11993c68ef75b8c85a32fe99e45d div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k6;

	@FindBy(css = "#viewedId73232cc1f72146560b61c2584a092468 div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k7;

	@FindBy(css = "#viewedId812274bb2faefd8e45fb5b5a53a2075b div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k8;

	@FindBy(css = "#viewedId8aa1ca76cf2d1cbce8aefb8d789c69fc div:nth-of-type(1) div:nth-of-type(1) div.logopath a:nth-of-type(1)")
	//@CacheLookup
	private WebElement k9;

	@FindBy(id = "company4")
	//@CacheLookup
	private WebElement kcura1;

	@FindBy(id = "company28")
	//@CacheLookup
	private WebElement kcura10;

	@FindBy(id = "company29")
	//@CacheLookup
	private WebElement kcura11;

	@FindBy(id = "company5")
	//@CacheLookup
	private WebElement kcura2;

	@FindBy(id = "company17")
	//@CacheLookup
	private WebElement kcura3;

	@FindBy(id = "company18")
	//@CacheLookup
	private WebElement kcura4;

	@FindBy(id = "company19")
	//@CacheLookup
	private WebElement kcura5;

	@FindBy(id = "company20")
	//@CacheLookup
	private WebElement kcura6;

	@FindBy(id = "company23")
	//@CacheLookup
	private WebElement kcura7;

	@FindBy(id = "company24")
	//@CacheLookup
	private WebElement kcura8;

	@FindBy(id = "company25")
	//@CacheLookup
	private WebElement kcura9;

	@FindBy(id = "company4")
	//@CacheLookup
	private WebElement kcuraCorporation1;

	@FindBy(id = "company19")
	//@CacheLookup
	private WebElement kcuraCorporation10;

	@FindBy(id = "company20")
	//@CacheLookup
	private WebElement kcuraCorporation11;

	@FindBy(id = "company20")
	//@CacheLookup
	private WebElement kcuraCorporation12;

	@FindBy(id = "company23")
	//@CacheLookup
	private WebElement kcuraCorporation13;

	@FindBy(id = "company23")
	//@CacheLookup
	private WebElement kcuraCorporation14;

	@FindBy(id = "company24")
	//@CacheLookup
	private WebElement kcuraCorporation15;

	@FindBy(id = "company24")
	//@CacheLookup
	private WebElement kcuraCorporation16;

	@FindBy(id = "company25")
	//@CacheLookup
	private WebElement kcuraCorporation17;

	@FindBy(id = "company25")
	//@CacheLookup
	private WebElement kcuraCorporation18;

	@FindBy(id = "company28")
	//@CacheLookup
	private WebElement kcuraCorporation19;

	@FindBy(id = "company4")
	//@CacheLookup
	private WebElement kcuraCorporation2;

	@FindBy(id = "company28")
	//@CacheLookup
	private WebElement kcuraCorporation20;

	@FindBy(id = "company29")
	//@CacheLookup
	private WebElement kcuraCorporation21;

	@FindBy(id = "company29")
	//@CacheLookup
	private WebElement kcuraCorporation22;

	@FindBy(id = "company4")
	//@CacheLookup
	private WebElement kcuraCorporation23;

	@FindBy(id = "company5")
	//@CacheLookup
	private WebElement kcuraCorporation24;

	@FindBy(id = "company17")
	//@CacheLookup
	private WebElement kcuraCorporation25;

	@FindBy(id = "company18")
	//@CacheLookup
	private WebElement kcuraCorporation26;

	@FindBy(id = "company19")
	//@CacheLookup
	private WebElement kcuraCorporation27;

	@FindBy(id = "company20")
	//@CacheLookup
	private WebElement kcuraCorporation28;

	@FindBy(id = "company23")
	//@CacheLookup
	private WebElement kcuraCorporation29;

	@FindBy(id = "company5")
	//@CacheLookup
	private WebElement kcuraCorporation3;

	@FindBy(id = "company24")
	//@CacheLookup
	private WebElement kcuraCorporation30;

	@FindBy(id = "company25")
	//@CacheLookup
	private WebElement kcuraCorporation31;

	@FindBy(id = "company28")
	//@CacheLookup
	private WebElement kcuraCorporation32;

	@FindBy(id = "company29")
	//@CacheLookup
	private WebElement kcuraCorporation33;

	@FindBy(id = "company5")
	//@CacheLookup
	private WebElement kcuraCorporation4;

	@FindBy(id = "company17")
	//@CacheLookup
	private WebElement kcuraCorporation5;

	@FindBy(id = "company17")
	//@CacheLookup
	private WebElement kcuraCorporation6;

	@FindBy(id = "company18")
	//@CacheLookup
	private WebElement kcuraCorporation7;

	@FindBy(id = "company18")
	//@CacheLookup
	private WebElement kcuraCorporation8;

	@FindBy(id = "company19")
	//@CacheLookup
	private WebElement kcuraCorporation9;

	@FindBy(css = "a[href='/jobs/dc-Kforce_Inc.-sort-date-jobs']")
	//@CacheLookup
	private WebElement kforceInc;

	@FindBy(id = "position5")
	//@CacheLookup
	private WebElement leadSoftwareEngineer1;

	@FindBy(id = "position5")
	//@CacheLookup
	private WebElement leadSoftwareEngineer2;

	@FindBy(css = "a[href='/jobs/dc-Leidos-sort-date-jobs']")
	//@CacheLookup
	private WebElement leidos;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(11) a.l")
	//@CacheLookup
	private WebElement less1;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(11) a.l")
	//@CacheLookup
	private WebElement less2;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(11) a.l")
	//@CacheLookup
	private WebElement less3;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.btn-link")
	//@CacheLookup
	private WebElement loginregister1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(3) a.btn-link")
	//@CacheLookup
	private WebElement loginregister2;

	@FindBy(css = "a[href='/jobs/l-Los_Angeles%2C_CA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement losAngelesCa;

	@FindBy(id = "position6")
	//@CacheLookup
	private WebElement mainframeComputerOperatorData1;

	@FindBy(id = "position6")
	//@CacheLookup
	private WebElement mainframeComputerOperatorData2;

	@FindBy(id = "position14")
	//@CacheLookup
	private WebElement microsoftBiDeveloper1;

	@FindBy(id = "position14")
	//@CacheLookup
	private WebElement microsoftBiDeveloper2;

	@FindBy(css = "a[href='/jobs/dc-Modis-sort-date-jobs']")
	//@CacheLookup
	private WebElement modis;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(6) a.m")
	//@CacheLookup
	private WebElement more1;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(6) a.m")
	//@CacheLookup
	private WebElement more2;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(6) a.m")
	//@CacheLookup
	private WebElement more3;

	@FindBy(css = "a[href='/jobs/djt-.Net_Developer-sort-date-jobs']")
	//@CacheLookup
	private WebElement netDeveloper;

	@FindBy(css = "a[href='/jobs/djt-.NET_Developer-sort-date-jobs']")
	//@CacheLookup
	private WebElement netDeveloper1;

	@FindBy(id = "position12")
	//@CacheLookup
	private WebElement netDeveloper2;

	@FindBy(id = "position12")
	//@CacheLookup
	private WebElement netDeveloper3;

	@FindBy(css = "a[href='/jobs/djt-Network_Engineer-sort-date-jobs']")
	//@CacheLookup
	private WebElement networkEngineer;

	@FindBy(css = "a[href='/jobs/l-New_York%2C_NY-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement newYorkNy;

	@FindBy(css = "a[href='mailto:newdicesupport@dice.com']")
	//@CacheLookup
	private WebElement newdicesupportdiceCom;

	@FindBy(css = "#search-results-control div:nth-of-type(1) div:nth-of-type(5) a:nth-of-type(2)")
	//@CacheLookup
	private WebElement no1;

	@FindBy(css = "#search-results-experiment div:nth-of-type(1) div:nth-of-type(3) a:nth-of-type(2)")
	//@CacheLookup
	private WebElement no2;

	@FindBy(css = "a[href='/jobs/dc-Northrop_Grumman-sort-date-jobs']")
	//@CacheLookup
	private WebElement northropGrumman;

	@FindBy(css = "#dice-services div.padding-container a:nth-of-type(2)")
	//@CacheLookup
	private WebElement openWeb;

	private final String pageLoadedText = "Overview At Relativity, we make great software that helps users organize data, discover the truth,";

	private final String pageUrl = "/jobs?searchid=9097517478437&stst=";

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(2) a.hjtype.tjth")
	//@CacheLookup
	private WebElement parttime1;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(2) a.bindChkBox")
	//@CacheLookup
	private WebElement parttime2;

	@FindBy(id = "diceMainDomainUS")
	//@CacheLookup
	private WebElement password1;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password10;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password11;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password12;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password13;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password14;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(6) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password15;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(7) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password16;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(8) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password17;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(9) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password18;

	@FindBy(css = "#positionTitle div:nth-of-type(3) div:nth-of-type(10) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password19;

	@FindBy(id = "diceMainDomainUK")
	//@CacheLookup
	private WebElement password2;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password20;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password21;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password22;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password23;

	@FindBy(css = "#locations div:nth-of-type(2) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password24;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password25;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password26;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password27;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password28;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password29;

	@FindBy(css = "#companySegments div:nth-of-type(2) div:nth-of-type(1) input.fchk.csck[type='checkbox']")
	//@CacheLookup
	private WebElement password3;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(6) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password30;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(7) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password31;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(8) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password32;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(9) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password33;

	@FindBy(css = "#locations div:nth-of-type(3) div:nth-of-type(10) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password34;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password35;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password36;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password37;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password38;

	@FindBy(css = "#companies div:nth-of-type(2) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password39;

	@FindBy(css = "#companySegments div:nth-of-type(2) div:nth-of-type(2) input.fchk.csck[type='checkbox']")
	//@CacheLookup
	private WebElement password4;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password40;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password41;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password42;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password43;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password44;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(6) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password45;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(7) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password46;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(8) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password47;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(9) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password48;

	@FindBy(css = "#companies div:nth-of-type(3) div:nth-of-type(10) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password49;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password5;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(1) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password50;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password51;

	@FindBy(css = "#jobTypes div:nth-of-type(2) div:nth-of-type(3) input.cfchk[type='checkbox']")
	//@CacheLookup
	private WebElement password52;

	@FindBy(css = "#jobTypes div:nth-of-type(3) div:nth-of-type(1) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password53;

	@FindBy(css = "#jobTypes div:nth-of-type(3) div:nth-of-type(2) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password54;

	@FindBy(css = "#jobTypes div:nth-of-type(3) div:nth-of-type(3) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password55;

	@FindBy(css = "#jobTypes div:nth-of-type(3) div:nth-of-type(4) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password56;

	@FindBy(css = "#jobTypes div:nth-of-type(4) div.filter-option input.cfchk[type='checkbox']")
	//@CacheLookup
	private WebElement password57;

	@FindBy(css = "#jobTypes div:nth-of-type(5) div:nth-of-type(1) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password58;

	@FindBy(css = "#jobTypes div:nth-of-type(5) div:nth-of-type(2) input.fchk.cjtype[type='checkbox']")
	//@CacheLookup
	private WebElement password59;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(2) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password6;

	@FindBy(css = "#telecommuteOption div:nth-of-type(2) div.filter-option input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password60;

	@FindBy(id = "mPassword")
	//@CacheLookup
	private WebElement password61;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(3) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password7;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(4) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password8;

	@FindBy(css = "#positionTitle div:nth-of-type(2) div:nth-of-type(5) input.fchk[type='checkbox']")
	//@CacheLookup
	private WebElement password9;

	@FindBy(id = "alertname")
	//@CacheLookup
	private WebElement pleaseEnterASearchTerm;

	@FindBy(id = "alertDisabled")
	//@CacheLookup
	private WebElement pleaseEnterAValidEmailEmaildomain1;

	@FindBy(id = "emailFrequency")
	//@CacheLookup
	private WebElement pleaseEnterAValidEmailEmaildomain2;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement postJobs1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement postJobs2;

	@FindBy(css = "a[href='/about/privacy']")
	//@CacheLookup
	private WebElement privacyPolicy;

	@FindBy(css = "a[href='/jobs/djt-Project_Manager-sort-date-jobs']")
	//@CacheLookup
	private WebElement projectManager;

	@FindBy(id = "position23")
	//@CacheLookup
	private WebElement qaEngineer1;

	@FindBy(id = "position23")
	//@CacheLookup
	private WebElement qaEngineer2;

	@FindBy(id = "position0")
	//@CacheLookup
	private WebElement ranEngineer1;

	@FindBy(id = "position0")
	//@CacheLookup
	private WebElement ranEngineer2;

	@FindBy(css = "a[href='/jobs/dcs-Recruiter-sort-date-jobs']")
	//@CacheLookup
	private WebElement recruiter;

	@FindBy(id = "register")
	//@CacheLookup
	private WebElement register;

	@FindBy(id = "sort-by-relevance-link")
	//@CacheLookup
	private WebElement relevance;

	@FindBy(css = "a[href='/jobs/djt-Remote_English_Teacher_%26%2345_Qkids-sort-date-jobs']")
	//@CacheLookup
	private WebElement remoteEnglishTeacherQkids;

	@FindBy(id = "reset-d")
	//@CacheLookup
	private WebElement reset1;

	@FindBy(id = "reset-m")
	//@CacheLookup
	private WebElement reset2;

	@FindBy(id = "return")
	//@CacheLookup
	private WebElement returnToDiceUs;

	@FindBy(css = "a[href='/jobs/dc-Robert_Half_Technology-sort-date-jobs']")
	//@CacheLookup
	private WebElement robertHalfTechnology;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement salaryPredictor1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(1) a")
	//@CacheLookup
	private WebElement salaryPredictor2;

	@FindBy(id = "position25")
	//@CacheLookup
	private WebElement salesforceDevelopmentManager1;

	@FindBy(id = "position25")
	//@CacheLookup
	private WebElement salesforceDevelopmentManager2;

	@FindBy(css = "a[href='/jobs/l-San_Diego%2C_CA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement sanDiegoCa;

	@FindBy(css = "a[href='/jobs/l-San_Francisco%2C_CA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement sanFranciscoCa;

	@FindBy(css = "a[href='/jobs/l-San_Jose%2C_CA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement sanJoseCa;

	@FindBy(css = "a[href='/jobs/l-Seattle%2C_WA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement seattleWa;

	@FindBy(id = "position19")
	//@CacheLookup
	private WebElement seniorDevopsEngineer1;

	@FindBy(id = "position19")
	//@CacheLookup
	private WebElement seniorDevopsEngineer2;

	@FindBy(id = "position29")
	//@CacheLookup
	private WebElement seniorManagerSoftware1;

	@FindBy(id = "position29")
	//@CacheLookup
	private WebElement seniorManagerSoftware2;

	@FindBy(id = "position10")
	//@CacheLookup
	private WebElement seniorReleaseManagerNewYork1;

	@FindBy(id = "position10")
	//@CacheLookup
	private WebElement seniorReleaseManagerNewYork2;

	@FindBy(css = "a[href='/jobs/djt-Senior_Software_Engineer-sort-date-jobs']")
	//@CacheLookup
	private WebElement seniorSoftwareEngineer;

	@FindBy(id = "position7")
	//@CacheLookup
	private WebElement serviceDeskTechnician1;

	@FindBy(id = "position7")
	//@CacheLookup
	private WebElement serviceDeskTechnician2;

	@FindBy(id = "signBtnId")
	//@CacheLookup
	private WebElement signIn;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement skillsCenter1;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement skillsCenter2;

	@FindBy(css = "#dice-services div.padding-container a:nth-of-type(4)")
	//@CacheLookup
	private WebElement skillsCenter3;

	@FindBy(css = "button.btn.btn-primary.skip-button")
	//@CacheLookup
	private WebElement skip;

	@FindBy(css = "#learn-about-dice div.padding-container a:nth-of-type(4)")
	//@CacheLookup
	private WebElement socialRecruiting;

	@FindBy(css = "a[href='/jobs/djt-Software_Developer-sort-date-jobs']")
	//@CacheLookup
	private WebElement softwareDeveloper;

	@FindBy(css = "a[href='/jobs/djt-Software_Engineer-sort-date-jobs']")
	//@CacheLookup
	private WebElement softwareEngineer1;

	@FindBy(id = "position4")
	//@CacheLookup
	private WebElement softwareEngineer2;

	@FindBy(id = "position4")
	//@CacheLookup
	private WebElement softwareEngineer3;

	@FindBy(id = "position28")
	//@CacheLookup
	private WebElement softwareEngineerInTestJava1;

	@FindBy(id = "position28")
	//@CacheLookup
	private WebElement softwareEngineerInTestJava2;

	@FindBy(id = "position9")
	//@CacheLookup
	private WebElement solutionsArchitectChicagoIl1;

	@FindBy(id = "position9")
	//@CacheLookup
	private WebElement solutionsArchitectChicagoIl2;

	@FindBy(css = "a[href='/jobs/l-Sunnyvale%2C_CA-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement sunnyvaleCa;

	@FindBy(css = "a[href='/jobs/djt-Systems_Administrator-sort-date-jobs']")
	//@CacheLookup
	private WebElement systemsAdministrator;

	@FindBy(css = "a[href='/jobs/djt-Systems_Engineer-sort-date-jobs']")
	//@CacheLookup
	private WebElement systemsEngineer;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement talentSolutions1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(2) a")
	//@CacheLookup
	private WebElement talentSolutions2;

	@FindBy(id = "smart-toggle-Career")
	//@CacheLookup
	private WebElement techCareers1;

	@FindBy(id = "smart-toggle-Career3")
	//@CacheLookup
	private WebElement techCareers2;

	@FindBy(id = "position24")
	//@CacheLookup
	private WebElement technicalBusinessAnalyst1;

	@FindBy(id = "position24")
	//@CacheLookup
	private WebElement technicalBusinessAnalyst2;

	@FindBy(id = "position16")
	//@CacheLookup
	private WebElement technicalWriterI1;

	@FindBy(id = "position16")
	//@CacheLookup
	private WebElement technicalWriterI2;

	@FindBy(css = "a[href='/jobs/djt-Technology_Lead_%26%2345_US-sort-date-jobs']")
	//@CacheLookup
	private WebElement technologyLeadUs;

	@FindBy(css = "a[href='/jobs/dc-TEKsystems%2C_Inc.-sort-date-jobs']")
	//@CacheLookup
	private WebElement teksystemsInc;

	@FindBy(css = "a[href='/about/terms_and_conditions']")
	//@CacheLookup
	private WebElement termsAndConditions;

	@FindBy(css = "a[href='/jobs/dc-The_Creative_Group-sort-date-jobs']")
	//@CacheLookup
	private WebElement theCreativeGroup;

	@FindBy(css = "a[href='http://media.dice.com/category/the-dice-report/']")
	//@CacheLookup
	private WebElement theDiceReport;

	@FindBy(css = "a[href='/jobs/dc-The_Judge_Group-sort-date-jobs']")
	//@CacheLookup
	private WebElement theJudgeGroup;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(4) a:nth-of-type(1)")
	//@CacheLookup
	private WebElement thirdParty1;

	@FindBy(css = "#jobTypes div:nth-of-type(4) div.filter-option a:nth-of-type(1)")
	//@CacheLookup
	private WebElement thirdParty2;

	@FindBy(css = "div:nth-of-type(4) div.container div:nth-of-type(1) button:nth-of-type(1)")
	//@CacheLookup
	private WebElement toggleNavigation;

	@FindBy(css = "a.social-icon.twitter")
	//@CacheLookup
	private WebElement twitter;

	@FindBy(css = "a[href='/jobs/dc-U.S._Tech_Solutions_Inc.-sort-date-jobs']")
	//@CacheLookup
	private WebElement uSTechSolutionsInc;

	@FindBy(css = "a[href='/jobs/l-Washington%2C_DC-sort-date-jobs?ft=true']")
	//@CacheLookup
	private WebElement washingtonDc;

	@FindBy(css = "a[href='/jobs/djt-Web_Developer-sort-date-jobs']")
	//@CacheLookup
	private WebElement webDeveloper;

	@FindBy(css = "a[href='http://www.dhigroupinc.com/careers/default.aspx']")
	//@CacheLookup
	private WebElement workAtDice;

	@FindBy(css = "a[href='http://info.yahoo.com/privacy/us/yahoo/yas/details.html']")
	//@CacheLookup
	private WebElement yahooAdvertisingSolutionsPrivacyPolicy;

	@FindBy(css = "a[href='/jobs/dtco-true-sort-date-jobs']")
	//@CacheLookup
	private WebElement yes1;

	@FindBy(css = "#search-results-control div:nth-of-type(1) div:nth-of-type(5) a:nth-of-type(1)")
	//@CacheLookup
	private WebElement yes2;

	@FindBy(css = "#search-results-experiment div:nth-of-type(1) div:nth-of-type(3) a:nth-of-type(1)")
	//@CacheLookup
	private WebElement yes3;

	@FindBy(id = "location")
	//@CacheLookup
	private WebElement zipCodeCityOrState1;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(1) input.typeChkBox[type='checkbox']")
	//@CacheLookup
	private WebElement zipCodeCityOrState2;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(2) input.typeChkBox[type='checkbox']")
	//@CacheLookup
	private WebElement zipCodeCityOrState3;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(3) input.typeChkBox[type='checkbox']")
	//@CacheLookup
	private WebElement zipCodeCityOrState4;

	@FindBy(css = "#searchJob div.form-group div:nth-of-type(4) span:nth-of-type(4) input.typeChkBox[type='checkbox']")
	//@CacheLookup
	private WebElement zipCodeCityOrState5;

}
