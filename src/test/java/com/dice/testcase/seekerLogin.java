/**
 * 
 */
package com.dice.testcase;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.dice.PageFactory.OR_SeekerLoginPage;
import com.dice.core.Page;
import com.dice.page.dashBoard;

/**
 * @author Avinash
 *
 */
public class seekerLogin extends Page {

	public seekerLogin(WebDriver driver) {
		super(driver);
	}

	/**
	 * @param args
	 */
	public static Properties config = new Properties();

	@Test
	public void login() throws IOException {

		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\dice\\config\\config");
		config.load(fis);

		String url = config.getProperty("US_URL") + "/dashboard";
		Page.driver.get(url);

		OR_SeekerLoginPage login = PageFactory.initElements(driver, OR_SeekerLoginPage.class);

		dashBoard dash = login.dologin("avi@auto.com", "qaqaqa12");
		dash.alertOutCheck();
		Page.topNav.logOut();
		// driver.close();
	}

}
