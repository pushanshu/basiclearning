/**
 * 
 */
package com.dice.testcase;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dice.PageFactory.OR_SeekerLoginPage;
import com.dice.core.Constants;
import com.dice.core.Page;
import com.dice.page.Djv;
import com.dice.page.dashBoard;
import com.dice.page.searchPage;

/**
 * @author avinash.sharma
 *
 */
public class QuickSignIn {
	public static Properties config = new Properties();
			@BeforeClass
			public void beforeClass() {
				Page.openBrowser();
				
				
			}

			@Test(priority =1)
			public void FulltimeSign() {
				Page.targetURL(Constants.FULLTIME);
				Djv djv = new Djv();
				djv.signInFulltime("avi@auto.com", "qaqaqa12");
			}
		
			@Test(priority =2)
			public void URLSign() {
				Page.targetURL(Constants.URLJOBS);
				Djv djv = new Djv();
				
				djv.signInUrl("avi@auto.com", "qaqaqa12");
			}
			@AfterClass
			private void browerclose() {
				Page.driver.close();

			} 
		
	
}
