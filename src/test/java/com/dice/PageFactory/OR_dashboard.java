/**
 * 
 */
package com.dice.PageFactory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

/**
 * @author Avinash
 *
 */
public class OR_dashboard {
	private int timeout = 30;
	@FindBy(xpath = "//*[@id=\"searchable-confirmation-dashboard\"]/div/div/div[3]/button[2]")
	@CacheLookup
	public WebElement popSearchableNo;
	@FindBy(xpath = "//*[@id=\"profilePicSecId\"]/div[3]/a")
	@CacheLookup
	public WebElement deleteProfile;

	@FindBy(xpath = "//*[@id=\"editProfile\"]")
	@CacheLookup
	public WebElement editProfileButton;

	@FindBy(xpath = "//*[@id=\"contactFirstNameInput\"]")
	@CacheLookup
	public WebElement firstName;

	@FindBy(css = "#contactLastNameInput")
	@CacheLookup
	public WebElement lastName;

	@FindBy(css = "#contactPhoneNumberInput")
	@CacheLookup
	public WebElement phoneNumber;

	@FindBy(css = "#contactLocationInput")
	@CacheLookup
	public WebElement location;

	@FindBy(css = "#contactPostalCodeInput")
	@CacheLookup
	public WebElement zipcode;

	@FindBy(css = "#contactCountryInput")
	@CacheLookup
	public WebElement country;

	@FindBy(css = "#desiredPositionInput")
	@CacheLookup
	public WebElement desiredposition;

	@FindBy(css = "#salaryInput")
	@CacheLookup
	public WebElement annualsalary;

	@FindBy(css = "#hourlyRateInput")
	@CacheLookup
	public WebElement hourlyRate;

	@FindBy(xpath = "#experienceInput")
	@CacheLookup
	public WebElement experience;

	@FindBy(linkText = "Done")
	@CacheLookup
	public WebElement doneButton;

	@FindBy(css = "#ln-ddv-jobs")
	@CacheLookup
	public WebElement jobstab;

	@FindBy(css = "#ln-ddv-alerts")
	@CacheLookup
	public WebElement alerttab;

	@FindBy(css = "#ln-ddv-settings")
	@CacheLookup
	public WebElement settingtab;

	@FindBy(css = "#savedJobs > div.row.row-details > div:nth-child(3) > h3 > button.btn.btn-link.job-filter-link.selected")
	@CacheLookup
	public WebElement jobsSaveJobs;

	@FindBy(css = "#savedJobs > div.row.row-details > div:nth-child(3) > h3 > button:nth-child(3)")
	@CacheLookup
	public WebElement jobsAppliedJobs;

	@FindBy(css = "#savedJobs > div.row.row-details > div:nth-child(3) > h3 > button:nth-child(3)")
	@CacheLookup
	public WebElement jobsexpiredJobs;

	@FindBys({ @FindBy(className = "col-md-12"), @FindBy(tagName = "h2") })
	List<WebElement> alerts;
	
	public void alertgetCheck() {
		System.out.println(alerts.size());
		 
		for (int i = 1; i<=alerts.size(); i=i+1)

		{

			System.out.println(alerts.get(i).getText());

		}

	
	}
		/*
	 * @FindBy(xpath = "//*[@id=\"contactFirstNameInput\"]")
	 * 
	 * @CacheLookup public WebElement firstName;
	 * 
	 * @FindBy(xpath = "//*[@id=\"contactFirstNameInput\"]")
	 * 
	 * @CacheLookup public WebElement firstName;
	 * 
	 * @FindBy(xpath = "//*[@id=\"contactFirstNameInput\"]")
	 * 
	 * @CacheLookup public WebElement firstName;
	 * 
	 * @FindBy(xpath = "//*[@id=\"contactFirstNameInput\"]")
	 * 
	 * @CacheLookup public WebElement firstName;
	 */

}
