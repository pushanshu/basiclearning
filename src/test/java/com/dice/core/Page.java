/**
 * 
 */
package com.dice.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.dice.page.TopNavigation;
import com.dice.utils.TestConfig;
import com.dice.utils.monitoringMail;

/**
 * @author Avinash
 *
 */
public class Page {

	public static WebDriver driver;
	public static String baseUrl;
	public static String baseUrlUS;
	public static String baseUrlUK;
	public static Properties Config = new Properties();
	public static Properties OR = new Properties();
	public static FileInputStream fis;
	/*
	 * public static Xls_Reader excel = new Xls_Reader(
	 * System.getProperty("user.dir")+
	 * File.separator+"src"+File.separator+"test"+File.separator+"java"+File.
	 * separator+"com"+File.separator+"dice"+File.separator+"testdata"+File.
	 * pathSeparator+"test.xlsx");
	 */ 
	public static Logger logs = Logger.getLogger("devpinoyLogger");
	public static TopNavigation topNav;
	public static monitoringMail mail = new monitoringMail();

	public Page(WebDriver driver) {
		Page.driver = driver;
	}

	@BeforeSuite
	public static void openBrowser() {
System.out.println("open brower started");
		if (driver == null) {

			try {
				fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator
						+ "test" + File.separator + "java" + File.separator + "com" + File.separator + "dice"
						+ File.separator + "config" + File.separator + "config.properties");
			} catch (FileNotFoundException e) {
				System.out.println(e);
				e.printStackTrace();
			}
			try {
				Config.load(fis);
			} catch (IOException e) {
				System.out.println("Fail to read properity file");
				e.printStackTrace();
			}
			logs.debug("Loaded the Config property file");

			if (Config.getProperty("browser").equals("firefox")) {

				driver = new FirefoxDriver();
				logs.debug("Loaded Firefox");

			} else if (Config.getProperty("browser").equals("ie")) {

				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				driver = new InternetExplorerDriver();

			} else if (Config.getProperty("browser").equals("chrome")) {

				System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().window().maximize();

			}
			baseUrlUK = Config.getProperty("UK_URL");
			baseUrlUS = Config.getProperty("US_URL");
			baseUrl = Config.getProperty("US_URL");
			driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
			// DbManager.setMysqlDbConnection();
			topNav = new TopNavigation(driver);

		}

	}

	public void delay() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void click(String key) {
	 * 
	 * driver.findElement(By.xpath(OR.getProperty(key))).click();
	 * System.out.println(key);
	 * 
	 * }
	 * 
	 * public static void input(String key, String value) {
	 * 
	 * driver.findElement(By.xpath(OR.getProperty(key))).sendKeys(value);
	 * 
	 * }
	 */
	// Test cleanup
	@AfterSuite
	public static void TeardownTest() throws AddressException, MessagingException {
		System.out.println("email sending process started");
		mail.sendMail(TestConfig.server, TestConfig.from, TestConfig.to, "Test Report",
				"Please find the attached report", TestConfig.reportPath, "Reports.zip");
		System.out.println("email sent ");
		driver.quit();
	}

	public static void targetURL(String target) {
		driver.navigate().to(baseUrl + target);

	}

	public static void targetURLUK(String target) {
		driver.navigate().to(baseUrlUK + target);

	}

	public static void targetURLUS(String target) {
		driver.navigate().to(baseUrlUS + target);

	}

	public static void BrowerCloseTest() {
		driver.close();
	}

	public static void switchToTab() {
		// Switching between tabs using CTRL + tab keys.
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");
		// Switch to current selected tab's content.
		driver.switchTo().defaultContent();
	}

}
