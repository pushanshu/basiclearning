package com.dice.page;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.ChromeDriverManager;



public class BaseClass {
	
	public static WebDriver driver;
	
	public  BaseClass(WebDriver driver){
		BaseClass.driver = driver;
	    ChromeDriverManager.getInstance().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.dice.com/jobs");
	}
	
}
