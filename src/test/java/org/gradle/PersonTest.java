package org.gradle;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

public class PersonTest {
    @Test
    public void canConstructAPersonWithAName() {
        Person person = new Person("Larry");
        AssertJUnit.assertEquals("Larry", person.getName());
    }
}
