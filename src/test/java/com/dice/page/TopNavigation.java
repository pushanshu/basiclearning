package com.dice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.dice.PageFactory.OR_TopNavigation;
import com.dice.core.Page;

public class TopNavigation extends Page {

	public TopNavigation(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	OR_TopNavigation tn = PageFactory.initElements(driver, OR_TopNavigation.class);

	public void logOut() {
	delay();
	WebElement UserName = tn.HeaderUserName;
		System.out.println("User Name = " + UserName.getAttribute("text").trim());
		tn.HeaderUserName.click();delay();
		tn.oSignOut.click();delay();
		System.out.println("Logout");
	}

	public void doSearch() {

	}

	public void goToHome() {

	}
}
