/**
 * 
 */
package com.dice.core;

/**
 * @author avinash.sharma
 *
 */
public class Constants {

	/**
	 * @param args
	 */

	public static final String FULLTIME = "/jobs/detail/Full-time-DHItestJob-AvinashGroup-Tanana-AK-99777/Avinash/258888?icid=sr1-1p&q=&l=99777";
	public static final String THIRDPARTY = "/jobs/detail/third-party-DHItestJob-AvinashGroup-Tanana-AK-99777/Avinash/1000?icid=sr2-1p&q=&l=99777";
	public static final String URLJOBS = "/jobs/detail/urldhitestjob-AvinashGroup-Tanana-AK-99777/Avinash/345377?icid=sr1-1p";
	public static final String SALARYCALCULATOR ="/salary-calculator";
	public static final String CAREERPATH ="/career-paths";
    public static final String SKILLS="/skills";
    
}
