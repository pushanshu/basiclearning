/**
 * 
 */
package com.dice.testcase;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dice.core.Constants;
import com.dice.core.Page;
import com.dice.page.Djv;

/**
 * @author avinash.sharma
 *
 */
public class QuickSignIn {
	public static Properties config = new Properties();

	@BeforeClass
	public void beforeClass() {
		Page.openBrowser();

	}

	/**
	 * 
	 */
	@Test(priority = 1)
	public void FulltimeSign() {
		Page.targetURLUS(Constants.FULLTIME);
		Djv djv = new Djv();
		djv.applyButton();
		djv.signInDjv("avi@auto.com", "qaqaqa12");
		djv.applicationCanel();
		Page.topNav.logOut();
	}

	@Test(priority = 2)
	public void ThirdPartytimeSign() {
		Page.targetURLUS(Constants.THIRDPARTY);
		Djv djv = new Djv();
		djv.applyButton();
		djv.signInDjv("avi@auto.com", "qaqaqa12");
		djv.applicationCanel();
		Page.topNav.logOut();
	}

	@Test(priority = 3)
	public void UrlJobsSign() {
		Page.targetURLUS(Constants.URLJOBS);
		Djv djv = new Djv();
		djv.applyButton();
		djv.signInDjv("avi@auto.com", "qaqaqa12");
		djv.Verifyurlcompany();
		Page.topNav.logOut();
	}

	@AfterClass
	private void BrowserClose(){ 
		Page.BrowerCloseTest();
	}

}
