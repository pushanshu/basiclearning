package com.dice.PageFactory;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author Avinash
 *
 */

public class OR_AdvanceSearch {
	private Map<String, String> data;
	private WebDriver driver;
	private int timeout = 15;

	@FindBy(css = "a[href='http://insights.dice.com/about-dice/']")
	@CacheLookup
	private WebElement aboutUs;

	@FindBy(css = "a.app-store-link.app-store.CMY_Link.CMY_Redirect.CMY_Valid")
	@CacheLookup
	private WebElement appStore;

	@FindBy(css = "a[href='/jobs/browsejobs']")
	@CacheLookup
	private WebElement browseJobs;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement c2hIndependent;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement c2hW2;

	@FindBy(id = "smart-toggle-Career-Explorer")
	@CacheLookup
	private WebElement careerExplorer1;

	@FindBy(id = "smart-toggle-Career-Explorer")
	@CacheLookup
	private WebElement careerExplorer2;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement careerPaths1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement careerPaths2;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(3) a.CMY_Valid.CMY_Link.CMY_Redirect")
	@CacheLookup
	private WebElement careerToolkitArticles1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(3) a.CMY_Link.CMY_Redirect.CMY_Valid")
	@CacheLookup
	private WebElement careerToolkitArticles2;

	@FindBy(css = "a[href='http://www.google.co.uk/doubleclick']")
	@CacheLookup
	private WebElement clickHere;

	@FindBy(id = "CloseCookieConsentPolicy")
	@CacheLookup
	private WebElement close;

	@FindBy(css = "a[href='http://media.dice.com/']")
	@CacheLookup
	private WebElement companyProfile;

	@FindBy(css = "a[href='http://techhub.dice.com/Dice_General-ContactUs_D.html']")
	@CacheLookup
	private WebElement contactSales;

	@FindBy(css = "a[href='/about/contactus']")
	@CacheLookup
	private WebElement contactUs;

	@FindBy(id = "continue")
	@CacheLookup
	private WebElement continueToDiceUk;

	@FindBy(css = "button.btn.btn-primary.pull-right")
	@CacheLookup
	private WebElement continueWithTheNewSite;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement contractCorptocorp;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement contractIndependent;

	@FindBy(css = "#jtype1 a input[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp1;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement contractToHireCorptocorp10;

	@FindBy(css = "#jtype9 a input.thParty[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp11;

	@FindBy(css = "#jtype2 a input[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp2;

	@FindBy(css = "input.Contracts")
	@CacheLookup
	private WebElement contractToHireCorptocorp3;

	@FindBy(css = "#jtype4 a input.Contract[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp4;

	@FindBy(css = "#jtype5 a input.Contract[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp5;

	@FindBy(css = "#jtype6 a input.Contract[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp6;

	@FindBy(css = "#jtype7 a input.Contract[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp7;

	@FindBy(css = "input.ThirdParty")
	@CacheLookup
	private WebElement contractToHireCorptocorp8;

	@FindBy(css = "#jtype8 a input.thParty[type='checkbox']")
	@CacheLookup
	private WebElement contractToHireCorptocorp9;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement contractW2;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement contracts;

	@FindBy(css = "a[href='http://www.dhigroupinc.com']")
	@CacheLookup
	private WebElement dhiService;

	@FindBy(css = "footer.dice-footer div:nth-of-type(1) div.row div:nth-of-type(1) a.logo.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement dice;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(3) a.CMY_Link.CMY_Invalid")
	@CacheLookup
	private WebElement diceCareersMobile4041;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(3) a.CMY_Invalid.CMY_Link")
	@CacheLookup
	private WebElement diceCareersMobile4042;

	@FindBy(css = "a[href='http://de.dice.com/']")
	@CacheLookup
	private WebElement diceDe;

	@FindBy(css = "a[href='http://sap.dice.com/']")
	@CacheLookup
	private WebElement diceSap;

	@FindBy(css = "a.diceUkAnchor.CMY_Redirect.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement diceUk;

	@FindBy(name = "sort")
	@CacheLookup
	private List<WebElement> distance1;

	@FindBy(name = "sort")
	@CacheLookup
	private List<WebElement> distance2;

	@FindBy(name = "sort")
	@CacheLookup
	private List<WebElement> distance3;

	@FindBy(id = "mEmail")
	@CacheLookup
	private WebElement email;

	@FindBy(css = "a.social-icon.facebook.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement facebook;

	@FindBy(id = "adv_search")
	@CacheLookup
	private WebElement findTechJobs;

	@FindBy(css = "#apply-job-modal-form div:nth-of-type(4) div:nth-of-type(1) p.pull-right a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement forgotPassword1;

	@FindBy(css = "#apply-job-modal-form div:nth-of-type(4) div:nth-of-type(4) p.pull-right a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement forgotPassword2;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement fulltime;

	@FindBy(css = "a.app-store-link.google-play.CMY_Link.CMY_Redirect.CMY_Valid")
	@CacheLookup
	private WebElement googlePlay;

	@FindBy(css = "a.social-icon.google-plus.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement googlePlus;

	@FindBy(css = "a[href='http://tools.google.com/dlpage/gaoptout?hl=en']")
	@CacheLookup
	private WebElement googlesPrivacySite;

	@FindBy(id = "agreePolicyModal")
	@CacheLookup
	private WebElement iAccept;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(3) a.CMY_Link.CMY_Redirect.CMY_Valid")
	@CacheLookup
	private WebElement insights1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(1) li:nth-of-type(3) a.CMY_Link.CMY_Redirect.CMY_Valid")
	@CacheLookup
	private WebElement insights2;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(1) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement jobSearch1;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(1) a.CMY_Valid.CMY_Link")
	@CacheLookup
	private WebElement jobSearch2;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.btn-link.CMY_Valid.CMY_Link")
	@CacheLookup
	private WebElement loginregister1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(3) a.btn-link.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement loginregister2;

	@FindBy(id = "for_com")
	@CacheLookup
	private WebElement multipleCompaniesMustBeSeparatedBy;

	@FindBy(css = "a[href='mailto:newdicesupport@dice.com']")
	@CacheLookup
	private WebElement newdicesupportdiceCom;

	@FindBy(css = "#dice-services div.padding-container a:nth-of-type(2)")
	@CacheLookup
	private WebElement openWeb;

	private final String pageLoadedText = "Use of this site is subject to certain";

	private final String pageUrl = "/jobs/advancedSearch.html";

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement parttime;

	@FindBy(id = "diceMainDomainUS")
	@CacheLookup
	private WebElement password1;

	@FindBy(id = "diceMainDomainUK")
	@CacheLookup
	private WebElement password2;

	@FindBy(id = "mPassword")
	@CacheLookup
	private WebElement password3;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(1) a.CMY_Valid.CMY_Link")
	@CacheLookup
	private WebElement postJobs1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(1) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement postJobs2;

	@FindBy(css = "a[href='/about/privacy']")
	@CacheLookup
	private WebElement privacyPolicy;

	@FindBy(id = "register")
	@CacheLookup
	private WebElement register;

	@FindBy(id = "return")
	@CacheLookup
	private WebElement returnToDiceUs;

	@FindBy(css = "#smart-toggle2 li:nth-of-type(1) a.CMY_Valid.CMY_Link")
	@CacheLookup
	private WebElement salaryPredictor1;

	@FindBy(css = "#smart-toggle4 li:nth-of-type(1) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement salaryPredictor2;

	@FindBy(id = "signBtnId")
	@CacheLookup
	private WebElement signIn;

	@FindBy(css = "#smart-toggle1 li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement skillsCenter1;

	@FindBy(css = "#smart-toggle3 li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement skillsCenter2;

	@FindBy(css = "#dice-services div.padding-container a:nth-of-type(4)")
	@CacheLookup
	private WebElement skillsCenter3;

	@FindBy(css = "button.btn.btn-primary.skip-button")
	@CacheLookup
	private WebElement skip;

	@FindBy(css = "#learn-about-dice div.padding-container a:nth-of-type(4)")
	@CacheLookup
	private WebElement socialRecruiting;

	@FindBy(css = "#menu-toggle div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement talentSolutions1;

	@FindBy(css = "#menu-toggle div:nth-of-type(2) ul:nth-of-type(2) li:nth-of-type(2) a.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement talentSolutions2;

	@FindBy(id = "smart-toggle-Career")
	@CacheLookup
	private WebElement techCareers1;

	@FindBy(id = "smart-toggle-Career3")
	@CacheLookup
	private WebElement techCareers2;

	@FindBy(css = "a[href='/about/terms_and_conditions']")
	@CacheLookup
	private WebElement termsAndConditions;

	@FindBy(css = "a[href='http://media.dice.com/category/the-dice-report/']")
	@CacheLookup
	private WebElement theDiceReport;

	@FindBy(name = "jtype")
	@CacheLookup
	private WebElement thirdParty;

	@FindBy(css = "button.btn.btn-default.dropdown-toggle")
	@CacheLookup
	private WebElement toggleDropdown;

	@FindBy(css = "div:nth-of-type(3) div.container div:nth-of-type(1) button:nth-of-type(1)")
	@CacheLookup
	private WebElement toggleNavigation;

	@FindBy(css = "a.social-icon.twitter.CMY_Link.CMY_Valid")
	@CacheLookup
	private WebElement twitter;

	@FindBy(css = "a[href='http://www.dhigroupinc.com/careers/default.aspx']")
	@CacheLookup
	private WebElement workAtDice;

	@FindBy(css = "a[href='http://info.yahoo.com/privacy/us/yahoo/yas/details.html']")
	@CacheLookup
	private WebElement yahooAdvertisingSolutionsPrivacyPolicy;

	@FindBy(id = "for_one")
	@CacheLookup
	private WebElement yes1;

	@FindBy(id = "for_all")
	@CacheLookup
	private WebElement yes2;

	@FindBy(id = "for_exact")
	@CacheLookup
	private WebElement yes3;

	@FindBy(id = "for_none")
	@CacheLookup
	private WebElement yes4;

	@FindBy(id = "for_jt")
	@CacheLookup
	private WebElement yes5;

	@FindBy(id = "for_loc")
	@CacheLookup
	private WebElement yes6;

	@FindBy(id = "telecommute2")
	@CacheLookup
	private WebElement yes7;

	public OR_AdvanceSearch() {
	}

	public OR_AdvanceSearch(WebDriver driver) {
		this();
		this.driver = driver;
	}

	public OR_AdvanceSearch(WebDriver driver, Map<String, String> data) {
		this(driver);
		this.data = data;
	}

	public OR_AdvanceSearch(WebDriver driver, Map<String, String> data, int timeout) {
		this(driver, data);
		this.timeout = timeout;
	}

	/**
	 * Click on About Us Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickAboutUsLink() {
		aboutUs.click();
		return this;
	}

	/**
	 * Click on App Store Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickAppStoreLink() {
		appStore.click();
		return this;
	}

	/**
	 * Click on Browse Jobs Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickBrowseJobsLink() {
		browseJobs.click();
		return this;
	}

	/**
	 * Click on C2h Independent Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickC2hIndependentLink() {
		c2hIndependent.click();
		return this;
	}

	/**
	 * Click on C2h W2 Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickC2hW2Link() {
		c2hW2.click();
		return this;
	}

	/**
	 * Click on Career Explorer Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerExplorer1Link() {
		careerExplorer1.click();
		return this;
	}

	/**
	 * Click on Career Explorer Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerExplorer2Link() {
		careerExplorer2.click();
		return this;
	}

	/**
	 * Click on Career Paths Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerPaths1Link() {
		careerPaths1.click();
		return this;
	}

	/**
	 * Click on Career Paths Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerPaths2Link() {
		careerPaths2.click();
		return this;
	}

	/**
	 * Click on Career Toolkit Articles Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerToolkitArticles1Link() {
		careerToolkitArticles1.click();
		return this;
	}

	/**
	 * Click on Career Toolkit Articles Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCareerToolkitArticles2Link() {
		careerToolkitArticles2.click();
		return this;
	}

	/**
	 * Click on Click Here Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickClickHereLink() {
		clickHere.click();
		return this;
	}

	/**
	 * Click on Close Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCloseButton() {
		close.click();
		return this;
	}

	/**
	 * Click on Company Profile Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickCompanyProfileLink() {
		companyProfile.click();
		return this;
	}

	/**
	 * Click on Contact Sales Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContactSalesLink() {
		contactSales.click();
		return this;
	}

	/**
	 * Click on Contact Us Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContactUsLink() {
		contactUs.click();
		return this;
	}

	/**
	 * Click on Continue To Dice Uk Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContinueToDiceUkButton() {
		continueToDiceUk.click();
		return this;
	}

	/**
	 * Click on Continue With The New Site Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContinueWithTheNewSiteButton() {
		continueWithTheNewSite.click();
		return this;
	}

	/**
	 * Click on Contract Corptocorp Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContractCorptocorpLink() {
		contractCorptocorp.click();
		return this;
	}

	/**
	 * Click on Contract Independent Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContractIndependentLink() {
		contractIndependent.click();
		return this;
	}

	/**
	 * Click on Contract To Hire Corptocorp Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContractToHireCorptocorp10Link() {
		contractToHireCorptocorp10.click();
		return this;
	}

	/**
	 * Click on Contract W2 Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContractW2Link() {
		contractW2.click();
		return this;
	}

	/**
	 * Click on Contracts Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickContractsLink() {
		contracts.click();
		return this;
	}

	/**
	 * Click on Dhi Service Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDhiServiceLink() {
		dhiService.click();
		return this;
	}

	/**
	 * Click on Dice Careers Mobile 404 Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceCareersMobile4041Link() {
		diceCareersMobile4041.click();
		return this;
	}

	/**
	 * Click on Dice Careers Mobile 404 Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceCareersMobile4042Link() {
		diceCareersMobile4042.click();
		return this;
	}

	/**
	 * Click on Dice De Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceDeLink() {
		diceDe.click();
		return this;
	}

	/**
	 * Click on Dice Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceLink() {
		dice.click();
		return this;
	}

	/**
	 * Click on Dice Sap Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceSapLink() {
		diceSap.click();
		return this;
	}

	/**
	 * Click on Dice Uk Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickDiceUkLink() {
		diceUk.click();
		return this;
	}

	/**
	 * Click on Facebook Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickFacebookLink() {
		facebook.click();
		return this;
	}

	/**
	 * Click on Find Tech Jobs Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickFindTechJobsButton() {
		findTechJobs.click();
		return this;
	}

	/**
	 * Click on Forgot Password Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickForgotPassword1Link() {
		forgotPassword1.click();
		return this;
	}

	/**
	 * Click on Forgot Password Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickForgotPassword2Link() {
		forgotPassword2.click();
		return this;
	}

	/**
	 * Click on Fulltime Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickFulltimeLink() {
		fulltime.click();
		return this;
	}

	/**
	 * Click on Google Play Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickGooglePlayLink() {
		googlePlay.click();
		return this;
	}

	/**
	 * Click on Google Plus Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickGooglePlusLink() {
		googlePlus.click();
		return this;
	}

	/**
	 * Click on Googles Privacy Site. Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickGooglesPrivacySiteLink() {
		googlesPrivacySite.click();
		return this;
	}

	/**
	 * Click on I Accept Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickIAcceptButton() {
		iAccept.click();
		return this;
	}

	/**
	 * Click on Insights Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickInsights1Link() {
		insights1.click();
		return this;
	}

	/**
	 * Click on Insights Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickInsights2Link() {
		insights2.click();
		return this;
	}

	/**
	 * Click on Job Search Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickJobSearch1Link() {
		jobSearch1.click();
		return this;
	}

	/**
	 * Click on Job Search Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickJobSearch2Link() {
		jobSearch2.click();
		return this;
	}

	/**
	 * Click on Loginregister Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickLoginregister1Link() {
		loginregister1.click();
		return this;
	}

	/**
	 * Click on Loginregister Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickLoginregister2Link() {
		loginregister2.click();
		return this;
	}

	/**
	 * Click on Newdicesupportdice.com Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickNewdicesupportdiceComLink() {
		newdicesupportdiceCom.click();
		return this;
	}

	/**
	 * Click on Open Web Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickOpenWebLink() {
		openWeb.click();
		return this;
	}

	/**
	 * Click on Parttime Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickParttimeLink() {
		parttime.click();
		return this;
	}

	/**
	 * Click on Post Jobs Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickPostJobs1Link() {
		postJobs1.click();
		return this;
	}

	/**
	 * Click on Post Jobs Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickPostJobs2Link() {
		postJobs2.click();
		return this;
	}

	/**
	 * Click on Privacy Policy Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickPrivacyPolicyLink() {
		privacyPolicy.click();
		return this;
	}

	/**
	 * Click on Register Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickRegisterLink() {
		register.click();
		return this;
	}

	/**
	 * Click on Return To Dice Us Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickReturnToDiceUsLink() {
		returnToDiceUs.click();
		return this;
	}

	/**
	 * Click on Salary Predictor Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSalaryPredictor1Link() {
		salaryPredictor1.click();
		return this;
	}

	/**
	 * Click on Salary Predictor Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSalaryPredictor2Link() {
		salaryPredictor2.click();
		return this;
	}

	/**
	 * Click on Sign In Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSignInButton() {
		signIn.click();
		return this;
	}

	/**
	 * Click on Skills Center Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSkillsCenter1Link() {
		skillsCenter1.click();
		return this;
	}

	/**
	 * Click on Skills Center Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSkillsCenter2Link() {
		skillsCenter2.click();
		return this;
	}

	/**
	 * Click on Skills Center Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSkillsCenter3Link() {
		skillsCenter3.click();
		return this;
	}

	/**
	 * Click on Skip Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSkipButton() {
		skip.click();
		return this;
	}

	/**
	 * Click on Social Recruiting Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickSocialRecruitingLink() {
		socialRecruiting.click();
		return this;
	}

	/**
	 * Click on Talent Solutions Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTalentSolutions1Link() {
		talentSolutions1.click();
		return this;
	}

	/**
	 * Click on Talent Solutions Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTalentSolutions2Link() {
		talentSolutions2.click();
		return this;
	}

	/**
	 * Click on Tech Careers Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTechCareers1Link() {
		techCareers1.click();
		return this;
	}

	/**
	 * Click on Tech Careers Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTechCareers2Link() {
		techCareers2.click();
		return this;
	}

	/**
	 * Click on Terms And Conditions Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTermsAndConditionsLink() {
		termsAndConditions.click();
		return this;
	}

	/**
	 * Click on The Dice Report Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTheDiceReportLink() {
		theDiceReport.click();
		return this;
	}

	/**
	 * Click on Third Party Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickThirdPartyLink() {
		thirdParty.click();
		return this;
	}

	/**
	 * Click on Toggle Dropdown Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickToggleDropdownButton() {
		toggleDropdown.click();
		return this;
	}

	/**
	 * Click on Toggle Navigation Button.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickToggleNavigationButton() {
		toggleNavigation.click();
		return this;
	}

	/**
	 * Click on Twitter Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickTwitterLink() {
		twitter.click();
		return this;
	}

	/**
	 * Click on Work At Dice Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickWorkAtDiceLink() {
		workAtDice.click();
		return this;
	}

	/**
	 * Click on Yahoo Advertising Solutions Privacy Policy Link.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch clickYahooAdvertisingSolutionsPrivacyPolicyLink() {
		yahooAdvertisingSolutionsPrivacyPolicy.click();
		return this;
	}

	/**
	 * Fill every fields in the page.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch fill() {
		setPassword1TextField();
		setPassword2TextField();
		setYes1TextField();
		setYes2TextField();
		setYes3TextField();
		setYes4TextField();
		setYes5TextField();
		setMultipleCompaniesMustBeSeparatedByTextField();
		setYes6TextField();
		setContractToHireCorptocorp1CheckboxField();
		setContractToHireCorptocorp2CheckboxField();
		setContractToHireCorptocorp3CheckboxField();
		setContractToHireCorptocorp4CheckboxField();
		setContractToHireCorptocorp5CheckboxField();
		setContractToHireCorptocorp6CheckboxField();
		setContractToHireCorptocorp7CheckboxField();
		setContractToHireCorptocorp8CheckboxField();
		setContractToHireCorptocorp9CheckboxField();
		setContractToHireCorptocorp11CheckboxField();
		/*
		 * setDistance1RadioButtonField(); setDistance2RadioButtonField();
		 * setDistance3RadioButtonField();
		 */ setYes7CheckboxField();
		setEmailTextField();
		setPassword3PasswordField();
		return this;
	}

	/**
	 * Fill every fields in the page and submit it to target page.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch fillAndSubmit() {
		fill();
		return submit();
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp11CheckboxField() {
		if (!contractToHireCorptocorp11.isSelected()) {
			contractToHireCorptocorp11.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp1CheckboxField() {
		if (!contractToHireCorptocorp1.isSelected()) {
			contractToHireCorptocorp1.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp2CheckboxField() {
		if (!contractToHireCorptocorp2.isSelected()) {
			contractToHireCorptocorp2.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp3CheckboxField() {
		if (!contractToHireCorptocorp3.isSelected()) {
			contractToHireCorptocorp3.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp4CheckboxField() {
		if (!contractToHireCorptocorp4.isSelected()) {
			contractToHireCorptocorp4.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp5CheckboxField() {
		if (!contractToHireCorptocorp5.isSelected()) {
			contractToHireCorptocorp5.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp6CheckboxField() {
		if (!contractToHireCorptocorp6.isSelected()) {
			contractToHireCorptocorp6.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp7CheckboxField() {
		if (!contractToHireCorptocorp7.isSelected()) {
			contractToHireCorptocorp7.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp8CheckboxField() {
		if (!contractToHireCorptocorp8.isSelected()) {
			contractToHireCorptocorp8.click();
		}
		return this;
	}

	/**
	 * Set Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setContractToHireCorptocorp9CheckboxField() {
		if (!contractToHireCorptocorp9.isSelected()) {
			contractToHireCorptocorp9.click();
		}
		return this;
	}

	/*    *//**
			 * Set default value to Distance Radio Button field.
			 *
			 * @return the OR_AdvanceSearch class instance.
			 */
	/*
	 * public OR_AdvanceSearch setDistance1RadioButtonField() { for (WebElement el :
	 * distance1) { Object distance1Value; if
	 * (el.getAttribute("value").equals(distance1Value)) { if (!el.isSelected()) {
	 * el.click(); } break; } } return this; }
	 * 
	 *//**
		 * Set default value to Distance Radio Button field.
		 *
		 * @return the OR_AdvanceSearch class instance.
		 */
	/*
	 * public OR_AdvanceSearch setDistance2RadioButtonField() { for (WebElement el :
	 * distance2) { if (el.getAttribute("value").equals(distance2Value)) { if
	 * (!el.isSelected()) { el.click(); } break; } } return this; }
	 * 
	 *//**
		 * Set default value to Distance Radio Button field.
		 *
		 * @return the OR_AdvanceSearch class instance.
		 */

	/*
	 * public OR_AdvanceSearch setDistance3RadioButtonField() { for (WebElement el :
	 * distance3) { if (el.getAttribute("value").equals(distance3Value)) { if
	 * (!el.isSelected()) { el.click(); } break; } } return this; }
	 * 
	 */ /**
		 * Set default value to Email Text field.
		 *
		 * @return the OR_AdvanceSearch class instance.
		 */
	public OR_AdvanceSearch setEmailTextField() {
		return setEmailTextField(data.get("EMAIL"));
	}

	/**
	 * Set value to Email Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setEmailTextField(String emailValue) {
		email.sendKeys(emailValue);
		return this;
	}

	/**
	 * Set default value to Multiple Companies Must Be Separated By Or E Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setMultipleCompaniesMustBeSeparatedByTextField() {
		return setMultipleCompaniesMustBeSeparatedByTextField(data.get("MULTIPLE_COMPANIES_MUST_BE_SEPARATED_BY"));
	}

	/**
	 * Set value to Multiple Companies Must Be Separated By Or E Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setMultipleCompaniesMustBeSeparatedByTextField(
			String multipleCompaniesMustBeSeparatedByValue) {
		multipleCompaniesMustBeSeparatedBy.sendKeys(multipleCompaniesMustBeSeparatedByValue);
		return this;
	}

	/**
	 * Set default value to Password Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword1TextField() {
		return setPassword1TextField(data.get("PASSWORD_1"));
	}

	/**
	 * Set value to Password Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword1TextField(String password1Value) {
		password1.sendKeys(password1Value);
		return this;
	}

	/**
	 * Set default value to Password Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword2TextField() {
		return setPassword2TextField(data.get("PASSWORD_2"));
	}

	/**
	 * Set value to Password Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword2TextField(String password2Value) {
		password2.sendKeys(password2Value);
		return this;
	}

	/**
	 * Set default value to Password Password field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword3PasswordField() {
		return setPassword3PasswordField(data.get("PASSWORD_3"));
	}

	/**
	 * Set value to Password Password field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setPassword3PasswordField(String password3Value) {
		password3.sendKeys(password3Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes1TextField() {
		return setYes1TextField(data.get("YES_1"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes1TextField(String yes1Value) {
		yes1.sendKeys(yes1Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes2TextField() {
		return setYes2TextField(data.get("YES_2"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes2TextField(String yes2Value) {
		yes2.sendKeys(yes2Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes3TextField() {
		return setYes3TextField(data.get("YES_3"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes3TextField(String yes3Value) {
		yes3.sendKeys(yes3Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes4TextField() {
		return setYes4TextField(data.get("YES_4"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes4TextField(String yes4Value) {
		yes4.sendKeys(yes4Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes5TextField() {
		return setYes5TextField(data.get("YES_5"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes5TextField(String yes5Value) {
		yes5.sendKeys(yes5Value);
		return this;
	}

	/**
	 * Set default value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes6TextField() {
		return setYes6TextField(data.get("YES_6"));
	}

	/**
	 * Set value to Yes Text field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes6TextField(String yes6Value) {
		yes6.sendKeys(yes6Value);
		return this;
	}

	/**
	 * Set Yes Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch setYes7CheckboxField() {
		if (!yes7.isSelected()) {
			yes7.click();
		}
		return this;
	}

	/**
	 * Submit the form to target page.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch submit() {
		clickIAcceptButton();
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp11CheckboxField() {
		if (contractToHireCorptocorp11.isSelected()) {
			contractToHireCorptocorp11.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp1CheckboxField() {
		if (contractToHireCorptocorp1.isSelected()) {
			contractToHireCorptocorp1.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp2CheckboxField() {
		if (contractToHireCorptocorp2.isSelected()) {
			contractToHireCorptocorp2.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp3CheckboxField() {
		if (contractToHireCorptocorp3.isSelected()) {
			contractToHireCorptocorp3.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp4CheckboxField() {
		if (contractToHireCorptocorp4.isSelected()) {
			contractToHireCorptocorp4.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp5CheckboxField() {
		if (contractToHireCorptocorp5.isSelected()) {
			contractToHireCorptocorp5.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp6CheckboxField() {
		if (contractToHireCorptocorp6.isSelected()) {
			contractToHireCorptocorp6.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp7CheckboxField() {
		if (contractToHireCorptocorp7.isSelected()) {
			contractToHireCorptocorp7.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp8CheckboxField() {
		if (contractToHireCorptocorp8.isSelected()) {
			contractToHireCorptocorp8.click();
		}
		return this;
	}

	/**
	 * Unset Contract To Hire Corptocorp Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetContractToHireCorptocorp9CheckboxField() {
		if (contractToHireCorptocorp9.isSelected()) {
			contractToHireCorptocorp9.click();
		}
		return this;
	}

	/**
	 * Unset Yes Checkbox field.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch unsetYes7CheckboxField() {
		if (yes7.isSelected()) {
			yes7.click();
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch verifyPageLoaded() {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getPageSource().contains(pageLoadedText);
			}
		});
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the OR_AdvanceSearch class instance.
	 */
	public OR_AdvanceSearch verifyPageUrl() {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getCurrentUrl().contains(pageUrl);
			}
		});
		return this;
	}
}
