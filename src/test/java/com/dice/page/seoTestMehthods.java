/**
 * 
 */
package com.dice.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.dice.PageFactory.OR_Djv;
import com.dice.PageFactory.OR_SearchPage;
import com.dice.PageFactory.OR_seoTestMehthods;
import com.dice.core.Page;
import com.dice.testcase.searchpage;

/**
 * @author Avinash
 *
 */
public class seoTestMehthods extends Page {
	public seoTestMehthods() {
		super(driver);
		}
	OR_seoTestMehthods seo = PageFactory.initElements(driver, OR_seoTestMehthods.class);
	public seoTestMehthods searchpageProperties() {
			String Pagetitle = driver.getTitle();
			System.out.println("Pagetitle  = " + Pagetitle);
			String metaTag=driver.findElement(By.xpath("//meta[@name='description']")).getAttribute("content");
			//String metaTag = seo.METATAG.getAttribute("content");
			System.out.println("meta tag descripton  = " + metaTag );
            String h1 = driver.findElement(By.tagName("h1")).getText();
            System.out.println("H1 Tag data = " + h1);
            return	PageFactory.initElements(driver, seoTestMehthods.class);
	}
		
}
