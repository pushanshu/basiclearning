/**
 * 
 */
package com.dice.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author avinash.sharma
 *
 */
public class OR_TopNavigation {
	private int timeout = 30;
	

	@FindBy(xpath = "(//button[@type='button'])[4]")
	public WebElement oApplyButton;
	@FindBy(id = "mEmailQuick")
	public WebElement oUserEmail;
	@FindBy(id = "mPasswordQuick")
	public WebElement oPassword;
	@FindBy(id = "easyapplyJob-log-in-btn")
	public WebElement oSignIn;
	
	
	//logout
	@FindBy(css="#smart-toggle-link")
	public WebElement dropdownSignOut;
	
	@FindBy(css="#smart-toggle > li:nth-child(6) > form > button")
	public WebElement oSignOut;
	
	@FindBy(xpath="//*[@id='seekerLogin']/li/a")
	public WebElement oLogout;
	
	@FindBy(linkText="Sign Out")
	public WebElement oLogoutNew;
	
	@FindBy(xpath="//a[@class='diceLogoLink gradientBG']")
	protected WebElement oDiceLogo;
	
	@FindBy(xpath="//*[@id='MyDice_1']/a/span")
	protected WebElement oMyDice;
	
	@FindBy(xpath="//*[@id='Dashboard_1']/a")
	protected WebElement oDashboard;

    @FindBy(xpath="//*[@id='My_Resumes_1']/a")
    protected WebElement oMyResume;
    
    


	

}
