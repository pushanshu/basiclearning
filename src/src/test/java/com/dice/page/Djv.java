package com.dice.page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.dice.PageFactory.OR_Djv;
import com.dice.PageFactory.OR_SearchPage;
import com.dice.core.Page;


public class Djv extends Page {
	public Djv() {
		super(driver);
		boolean acceptNextAlert = true;
			}
	OR_Djv djv = PageFactory.initElements(driver, OR_Djv.class);
/**
 * 
 * @param email
 * @param password
 * @return
 */
	public Djv signInFulltime(String email,String password) {
		djv.oApplyButton.click();
		delay();
		djv.oUserEmail.clear();
		djv.oUserEmail.sendKeys(email);
		djv.oPassword.clear();
		djv.oPassword.sendKeys(password);
		djv.oSignIn.click();delay();
		djv.oApplicationCanel.click();delay();
		Page.topNav.SignOut();
		
		return PageFactory.initElements(driver, Djv.class);
		}
	public Djv signInUrl(String email,String password) {
		djv.oApplyButton.click();
		delay();
		djv.oUserEmail.clear();
		djv.oUserEmail.sendKeys(email);
		djv.oPassword.clear();
		djv.oPassword.sendKeys(password);
		djv.oSignIn.click();
		Page.topNav.SignOut();
		
		return PageFactory.initElements(driver, Djv.class);
		}
	
	/*private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }*/
	
	public void delay(){
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}


