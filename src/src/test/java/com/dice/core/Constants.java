/**
 * 
 */
package com.dice.core;

/**
 * @author avinash.sharma
 *
 */
public class Constants {

	/**
	 * @param args
	 */

	public static final String FULLTIME = "/jobs/detail/Full-time-DHItestJob-AvinashGroup-Tanana-AK-99777/Avinash/258888?icid=sr1-1p&q=&l=99777";
	public static final String THIRDPARTY = "/jobs/detail/third-party-DHItestJob-AvinashGroup-Tanana-AK-99777/Avinash/1000?icid=sr2-1p&q=&l=99777";
	public static final String URLJOBS = "jobs/detail/urlDHItestJob-AvinashGroup-Tanana-AK-99777/Avinash/342024?icid=sr1-1p&q=&l=99777";
	public static final String JOBS = "/jobs";

}
